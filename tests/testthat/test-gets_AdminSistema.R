test_that("Token", {
  Token(usuario, password)
  expect_match(getToken(), "\\S+")
})

test_that("Usuarios", {
  usuarios <- Usuarios() %>% expect_s3_class("data.frame")
  usuarios %>% expect_named(c("email", "password", "programa", "rol"))
  expect_true("gerente" %in% usuarios$rol)
  usuarios$email %>% expect_match("^\\S+@\\S+\\.\\S+$")
  usuarios$password %>% is.na %>% all %>% expect_true
})

test_that("Programas", {
  programas <- Programas() %>% expect_s3_class("data.frame")
  programas %>% expect_named(c("id", "nombre"))
  programas$id %>% unique %>% expect_setequal(programas$id)
  programas$nombre %>% unique %>% expect_setequal(programas$nombre)
})

test_that("Roles", {
  roles <- Roles() %>% expect_s3_class("data.frame")
  roles %>% expect_named(c("rol", "descripcion"))
  nrow(roles) %>% expect_equal(5)
  roles$rol %>% unique %>% expect_setequal(roles$rol)
  expect_true(all(c("gerente", "analista", "recolector", "administrador", "investigador/mejorador") %in% roles$rol))
})
