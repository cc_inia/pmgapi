#' Realiza un llamado POST
#'
#' @keywords internal
#' @importFrom jsonlite fromJSON
#' @importFrom httr GET stop_for_status content add_headers
#' @import dplyr
#' @param clase La clase que se está consultando
#' @param body El identificador que se requiere en el Get
#' @return data.frame con el objeto solicitado
#' @examples
#' \dontrun{pmgPost("Cultivos")}
pmgPost <- function(clase, body) {
  .tkn <- getToken()
  r <- POST(
    url = paste0(api_url, "/", clase),
    config = add_headers(Authorization = paste('Bearer', .tkn)),
    body = body,
    encode = "json"
  ) %>%
    stop_for_status %>%
    content("text") %>%
    `Encoding<-`("UTF-8") %>%
    fromJSON(simplifyVector = TRUE)

  r
}
